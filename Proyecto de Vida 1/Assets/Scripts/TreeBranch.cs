﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Tree branch - La usamos para navegar atraves de los menus generando un arbol, cada prefab debe contener este script para dar refrencia al siguiente creandolo en tiempo de ejecucion.
/// </summary>
public class TreeBranch : MonoBehaviour 
{
	[Tooltip("Velocidad con la que se desvanece y aparece el siguiente objeto.")]
	public float fadeSpeed = 3.0f;
	[Tooltip("Sera nuestra lista de ramas (prefabPanel).")]
	public List<GameObject> prefabBranch;
	[Tooltip("Sera nuestra lista de popups (prefabPanel).")]
	public List<GameObject> prefabPopup;

	/// FORMULAS USADAS
	/// [Tiempo = Distancia/Velocidad],[Distancia = Velocidad * Tiempo],[Velocidad=Distancia/Tiempo].
	
	/// <summary>
	/// Raises the click to event.
	/// </summary>
	/// <param name="lnBranchIndex">Branch index.</param>
	public void OnClick(int lnBranchIndex)
	{
#if DEBUG
		if( prefabBranch.Count == 0 )
		{
			Debug.LogError("[TreeBranch]: No hay Branches para ser usados en la funcion OnClick." );
			return;
		}

		if(lnBranchIndex >= prefabBranch.Count)
		{
			Debug.LogError("[TreeBranch]: El indice del Branch dado en la funcion OnClick esta fuera del indice. Indice: " + lnBranchIndex.ToString() );
			return;
		}
#endif
		InstantiateElement ( prefabBranch[lnBranchIndex], false);//<< Intercambiamos
	}

	/// <summary>
	/// Raises the click popup event.
	/// </summary>
	/// <param name="lnPopupIndex">Ln popup index.</param>
	/// <param name="lcTagData">Tag data.</param>
	public void OnClickPopup(int lnPopupIndex)
	{
#if DEBUG
		if( prefabPopup.Count == 0 )
		{
			Debug.LogError("[TreeBranch]: No hay Popups para ser usados en la funcion OnClickPopup." );
			return;
		}
		
		if(lnPopupIndex >= prefabPopup.Count)
		{
			Debug.LogError("[TreeBranch]: El indice del Popup dado en la funcion OnClickPopup esta fuera del indice. Indice: " + lnPopupIndex.ToString() );
			return;
		}
#endif
		InstantiateElement(prefabPopup[lnPopupIndex], true);
	}

	/// <summary>
	/// Instantiates the element.
	/// </summary>
	/// <param name="loGameObject">Lo game object.</param>
	private void InstantiateElement(GameObject loGameObject, bool lbIsPopup)
	{
		GameObject loGameObjectNew = Instantiate (loGameObject);
		loGameObjectNew.transform.SetParent(gameObject.transform.parent,false);

		FadeIn (loGameObjectNew, lbIsPopup);//<< Realizamos el FadeIn.
	}

	/// <summary>
	/// Crosses the fade game objects.
	/// </summary>
	///	 <param name="loCurrent">Current.</param>
	/// <param name="loNext">Next.</param>
	private void FadeIn(GameObject loGameObject, bool lbIsPopup)
	{
		CanvasGroup loCanvasGroup = loGameObject.GetComponent<CanvasGroup> ();

#if DEBUG
		if( loCanvasGroup == null ){ Debug.Log("[TreeBranch] No se encontro el CanvasGroup para realizar CrossFade en el gameObject: " + loGameObject.name); return; }
#else
		if( loCanvasGroup == null ) return;
#endif

		//<< Se va a borrar el elemento en el tiempo designado para realizar el fade.
		StartCoroutine (CoroutineFadeTransition(true,loCanvasGroup, gameObject, fadeSpeed, !lbIsPopup));
	}

	public void OnClickFadeOutDestroyPopup()
	{
		CanvasGroup loCanvasGroup = gameObject.GetComponent<CanvasGroup> ();//<< Este elemento debe ser el Popup.
		
#if DEBUG
		if( loCanvasGroup == null ){ Debug.Log("[TreeBranch] No se encontro el CanvasGroup para realizar FadeOut en el gameObject: " + gameObject.name); return; }
#else
		if( loCanvasGroup == null ) return;
#endif

		//<< Se va a borrar el elemento en el tiempo designado para realizar el fade.
		StartCoroutine (CoroutineFadeTransition(false,loCanvasGroup, gameObject, fadeSpeed, true));
	}

	/// <summary>
	/// Coroutines to fade in canvas group.
	/// </summary>
	/// <returns>Nothing.</returns>
	/// <param name="loCanvasGroup">The canvas group.</param>
	IEnumerator CoroutineFadeTransition(bool IsFadeIn, CanvasGroup loCanvasGroup, GameObject loToDestroy, float lnSpeed, bool lbDestroy)
	{
		loCanvasGroup.interactable = false;	//<< Decimos que no podemos interactuar durante la transicion.

		if (IsFadeIn) 
		{
			float lnStep = 0.0f;
			while (lnStep < 1.0f) 
			{
				loCanvasGroup.alpha = lnStep;			//<< Nuestro Alpha establecido con el paso del tiempo.
				lnStep += fadeSpeed * Time.deltaTime;	//<< [Distancia = Velocidad * Tiempo]
				yield return null;
			}

			loCanvasGroup.alpha = 1.0f;					//<< Establecemos el alpha al 100%.
		}
		else//<< FadeOut
		{
			float lnStep = 1.0f;
			while (lnStep > 0.0f)
			{
				loCanvasGroup.alpha = lnStep;			//<< Nuestro Alpha establecido con el paso del tiempo.
				lnStep -= fadeSpeed * Time.deltaTime;	//<< [Distancia = Velocidad * Tiempo]
				yield return null;
			}
			
			loCanvasGroup.alpha = 0.0f;					//<< Establecemos el alpha al 0%.
		}

		loCanvasGroup.interactable = true;		//<< Indicamos que ya podemos interactuar con los elementos.
		if( lbDestroy ) Destroy (loToDestroy);	//<< Finalmente destruimos nuestro objeto de atras.
	}
}