﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class Animation2DScale : Animation2DTransition 
{
	public Vector3 scaleStart;
	public Vector3 scaleFinal;
	
	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		StartCoroutine (CoroutineTransition (loRectTransform, scaleStart, scaleFinal, gnSpeed, preDelayTime, posDelayTime));
	}
	
	IEnumerator CoroutineTransition(RectTransform loRectTransform, Vector3 loScaleStart, Vector3 loScaleFinal, float lnSpeed, float lnPreDelay, float lnPosDelay)
	{
		isActive = true;
		loRectTransform.localScale = loScaleStart;
		yield return new WaitForSeconds (lnPreDelay);
		float lnDelta = 0.0f;
		while (lnDelta < 1.0f)
		{
			lnDelta += lnSpeed * Time.deltaTime;//<< Actualizamos el delta.
			loRectTransform.transform.localScale = Vector3.Lerp (loScaleStart, loScaleFinal, lnDelta);//<< Nueva posicion.
			yield return null;//<< Esperamos siguiente actualizacion.
		}
		loRectTransform.localScale = loScaleFinal;
		yield return new WaitForSeconds (lnPosDelay);
		isActive = false;
	}
	
	public void SetStart()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		scaleStart = loRectTransform.localScale;
	}
	
	public void SetFinal()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		scaleFinal = loRectTransform.localScale;
	}
}
