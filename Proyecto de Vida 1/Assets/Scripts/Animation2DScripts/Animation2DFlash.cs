﻿using UnityEngine;
using System.Collections;

public class Animation2DFlash : Animation2DTransition
{
	[Tooltip("Alpha inicial normalizado.")]
	public float alphaStart=0.0f;
	[Tooltip("Alpha final normalizado.")]
	public float alphaFinal=1.0f;
	
	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		CanvasRenderer loCanvasRenderer = gameObject.GetComponent<CanvasRenderer> ();
		StartCoroutine (CoroutineTransition (loCanvasRenderer, alphaStart, alphaFinal, gnSpeed*0.5f, preDelayTime, posDelayTime));
	}
	
	IEnumerator CoroutineTransition(CanvasRenderer loCanvasRenderer, float lnAlphaStart, float lnAlphaFinal, float lnSpeed, float lnPreDelay, float lnPosDelay)
	{
		isActive = true;
		loCanvasRenderer.SetAlpha (lnAlphaStart);
		yield return new WaitForSeconds (lnPreDelay);
		float lnDelta = lnAlphaStart;
		
		if (lnAlphaStart < lnAlphaFinal) 
		{
			while (lnDelta < lnAlphaFinal) 
			{
				lnDelta += lnSpeed * Time.deltaTime;		//<< Actualizamos el delta.
				loCanvasRenderer.SetAlpha (lnDelta);
				yield return null;							//<< Esperamos siguiente actualizacion.
			}
		}
		else 
		{
			while (lnDelta > lnAlphaFinal) 
			{
				lnDelta -= lnSpeed * Time.deltaTime;		//<< Actualizamos el delta.
				loCanvasRenderer.SetAlpha (lnDelta);
				yield return null;							//<< Esperamos siguiente actualizacion.
			}
		}

		float lnTemp = lnAlphaStart;
		lnAlphaStart = lnAlphaFinal;
		lnAlphaFinal = lnTemp;

		loCanvasRenderer.SetAlpha (lnAlphaStart);

		if (lnAlphaStart < lnAlphaFinal) 
		{
			while (lnDelta < lnAlphaFinal) 
			{
				lnDelta += lnSpeed * Time.deltaTime;		//<< Actualizamos el delta.
				loCanvasRenderer.SetAlpha (lnDelta);
				yield return null;							//<< Esperamos siguiente actualizacion.
			}
		}
		else 
		{
			while (lnDelta > lnAlphaFinal) 
			{
				lnDelta -= lnSpeed * Time.deltaTime;		//<< Actualizamos el delta.
				loCanvasRenderer.SetAlpha (lnDelta);
				yield return null;							//<< Esperamos siguiente actualizacion.
			}
		}
		
		loCanvasRenderer.SetAlpha (lnAlphaFinal);
		yield return new WaitForSeconds (lnPosDelay);
		isActive = false;
	}
}
