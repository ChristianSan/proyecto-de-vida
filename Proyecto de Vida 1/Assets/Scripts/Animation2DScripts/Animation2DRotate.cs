﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(RectTransform) )]
public class Animation2DRotate : Animation2DTransition 
{
	public Quaternion rotationStart;
	public Quaternion rotationFinal;
	
	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		StartCoroutine (CoroutineTransition (loRectTransform, rotationStart, rotationFinal, gnSpeed, preDelayTime, posDelayTime));
	}
	
	IEnumerator CoroutineTransition(RectTransform loRectTransform, Quaternion loRotationStart, Quaternion loRotationFinal, float lnSpeed, float lnPreDelay, float lnPosDelay)
	{
		isActive = true;
		loRectTransform.localRotation = loRotationStart;
		yield return new WaitForSeconds (lnPreDelay);
		float lnDelta = 0.0f;
		while (lnDelta < 1.0f)
		{
			lnDelta += lnSpeed * Time.deltaTime;//<< Actualizamos el delta.
			loRectTransform.localRotation = Quaternion.Lerp (loRotationStart, loRotationFinal, lnDelta);//<< Nueva posicion.
			yield return null;//<< Esperamos siguiente actualizacion.
		}
		loRectTransform.localRotation = loRotationFinal;
		yield return new WaitForSeconds (lnPosDelay);
		isActive = false;
	}
	
	public void SetStart()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		rotationStart = loRectTransform.localRotation;
	}
	
	public void SetFinal()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		rotationFinal = loRectTransform.localRotation;
	}
}
