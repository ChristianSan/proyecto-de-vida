﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// Referencias:
/// http://docs.unity3d.com/ScriptReference/Material-mainTextureOffset.html

/// <summary>
/// Animation2 DUV scroll.
/// </summary>
public class Animation2DUVScroll : Animation2DTransition
{
	private RawImage goRawImage;
	private float lnDelta = 0.0f;
	private float lnStep = 0.0f;

	public Vector2 directionUV = new Vector2( 1.0f, 0.0f );
	
	// Use this for initialization
	public override void Start ()
	{
		base.Start ();

		goRawImage = GetComponent<RawImage>();
		//Material loMaterial = loRawImage.uvRect;
		//loMaterial.mainTextureOffset


		//loImage.
		//CanvasRenderer loCanvasRenderer = GetComponent<CanvasRenderer>();
		//Material loMaterial = loCanvasRenderer.GetMaterial ();

//		if (loMaterial == null)
//			Debug.Log ("No hay meterial!!");
//		else 
//		{
//			Debug.Log("Pre X: " + loMaterial.mainTextureOffset.x.ToString());
//			Debug.Log("Pre Y: " + loMaterial.mainTextureOffset.y.ToString());
//			loMaterial.mainTextureOffset.Set(0.1f, 0.3f);
//			//loMaterial.mainTextureOffset = new Vector2(0.2f, 0.4f);
//			Debug.Log("Pos X: " + loMaterial.mainTextureOffset.x.ToString());
//			Debug.Log("Pos Y: " + loMaterial.mainTextureOffset.y.ToString());
//		}

		//StartCoroutine (CoroutineTransition (loRawImage, directionUV, gnSpeed, preDelayTime, posDelayTime));
	}

	public void Update()
	{
		lnStep = gnSpeed * Time.deltaTime;//<< Actualizamos el delta.
		lnDelta += lnStep;//<< Actualizamos el delta.
		//.GetMaterial().mainTextureOffset += loDirection * lnStep;//<< Nueva posicion.
		if (goRawImage.uvRect == null) goRawImage.uvRect = new Rect (0,0,1,1);

		//goRawImage.uvRect.Set()
		Rect loRect = goRawImage.uvRect;
		loRect.x += lnStep * directionUV.x;
		loRect.y += lnStep * directionUV.y;
		//loRawImage.uvRect.set
		goRawImage.uvRect = loRect;
	}
	
	IEnumerator CoroutineTransition(RawImage loRawImage, Vector2 loDirection, float lnSpeed, float lnPreDelay, float lnPosDelay)
	{
		isActive = true;
		yield return new WaitForSeconds (lnPreDelay);
		float lnDelta = 0.0f;
		float lnStep = 0.0f;
		while (lnDelta < 1.0f)
		{
			lnStep = lnSpeed * Time.deltaTime;//<< Actualizamos el delta.
			lnDelta += lnStep;//<< Actualizamos el delta.
			 //.GetMaterial().mainTextureOffset += loDirection * lnStep;//<< Nueva posicion.
			Rect loRect = loRawImage.uvRect;
			loRect.x += lnStep * loDirection.x;
			loRect.y += lnStep * loDirection.y;
			//loRawImage.uvRect.set
			loRawImage.uvRect = loRect;
			yield return null;//<< Esperamos siguiente actualizacion.
		}
		yield return new WaitForSeconds (lnPosDelay);
		isActive = false;
	}
}