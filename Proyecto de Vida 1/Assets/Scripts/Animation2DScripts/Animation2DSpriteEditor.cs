﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Animation2DSprite))]
public class Animation2DSpriteEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		Animation2DSprite loObject = (Animation2DSprite)target;
		if(GUILayout.Button("Rellena los Sprites"))
		{
			loObject.PopulateList();
		}
	}
}