﻿using UnityEngine;
using System.Collections;

public class Animation2DTransition : MonoBehaviour 
{
	protected float gnSpeed;	//<< Velocidad a la que realizaremos la animacion.

	[Tooltip("Indica cuando la transicion se encuentra activa.")]
	public bool isActive=false;
	[Tooltip("Es el tiempo para realizar la transicion (segundos).")]
	public float time=1.0f;
	[Tooltip("Es el tiempo para realizar la transicion (segundos).")]
	public float preDelayTime=0.0f;
	[Tooltip("Es el tiempo para realizar la transicion (segundos).")]
	public float posDelayTime=0.0f;

	public virtual void Start()
	{
		gnSpeed = 1.0f / time;//<< Determinamos la velocidad con base en el tiempo asignado.
	}

	IEnumerator CoroutineTransition(float lnTime,float lnPreDelay,float lnPosDelay)
	{
		isActive = true;
		yield return new WaitForSeconds(lnTime+lnPreDelay+lnPosDelay);
		isActive = false;
	}
}