﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Animation2DTranslate))]
public class Animation2DTranslateEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		Animation2DTranslate loObject = (Animation2DTranslate)target;
		if(GUILayout.Button("Set Start"))
		{
			loObject.SetStart();
		}
		else if(GUILayout.Button("Set Final"))
		{
			loObject.SetFinal();
		}
	}
}