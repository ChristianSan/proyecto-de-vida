﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(Image))]
public class Animation2DSprite : Animation2DTransition
{
	private float gnStartPreDelayTime=0.0f;
	private float gnStartPosDelayTime=0.0f;
	private int gnIndexCount = 0;
	private float gnDelta = 0.0f;
	private Image goImage;
	private bool gbIsInactive=false;

	[Tooltip("Velocidad con la que se desvanece y aparece el siguiente objeto.")]
	public bool isLoop = true;
	[Tooltip("Es el tiempo de vida de nuestro sprite.")]
	public float lifeTime = 1.0f;
	[Tooltip("Sprites que forman nuestra animacion.")]
	public List<Sprite> sprites;

	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		goImage = gameObject.GetComponent<Image> ();
#if DEBUG
		if(goImage == null){ Debug.Log("Animation2DSprite"); return; }
#endif
		if (isLoop) isActive = false;
		else isActive = true;
		if (sprites.Count == 0) gnDelta=2.0f;
		else goImage.sprite = sprites [0];
		gnIndexCount = sprites.Count - 1;
		gnStartPreDelayTime = Time.time + preDelayTime;
	}

	public void FixedUpdate()
	{
		if (gbIsInactive) return;
		if (gnStartPreDelayTime > Time.time) return;

		if (gnDelta <= 1.0f)
		{
			gnDelta += gnSpeed * Time.deltaTime;//<< Actualizamos el delta.
			int lnIndex = Mathf.FloorToInt (gnDelta * gnIndexCount);
			if( lnIndex <= gnIndexCount ) goImage.overrideSprite = sprites [lnIndex];
			if( lnIndex >= gnIndexCount )//<< Llegamos al final de la animacion.
			{
				if (isLoop) 
				{
					gnDelta = 0.0f;
				}
				else
				{
					gnDelta=2.0f;
					gnStartPosDelayTime = Time.time + posDelayTime;
				}
			}
			return;
		}

		if( gnStartPosDelayTime > Time.time ) return;
		isActive = false;
		gbIsInactive = true;
	}

#if UNITY_EDITOR
	public void PopulateList()
	{
		//<< http://docs.unity3d.com/ScriptReference/AssetDatabase.FindAssets.html
		Image loImage = gameObject.GetComponent<Image> ();
		string[] lcStrings = loImage.sprite.name.Split (new char[]{'_'}, System.StringSplitOptions.None);
		string lcName = lcStrings [0];

		sprites.Clear ();
		string[] lcPaths = AssetDatabase.FindAssets( lcName + "_ t:Sprite");
		foreach (var item in lcPaths) 
		{
			Sprite loSprite = EditorGUIUtility.Load(AssetDatabase.GUIDToAssetPath(item)) as Sprite;
			if(loSprite != null ) sprites.Add (loSprite);
		}
	}
#endif
}
