﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(RectTransform) )]
public class Animation2DTranslate : Animation2DTransition 
{
	public Vector3 positionStart;
	public Vector3 positionFinal;
	
	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		StartCoroutine (CoroutineTransition (loRectTransform, positionStart, positionFinal, gnSpeed, preDelayTime, posDelayTime));
	}

	IEnumerator CoroutineTransition(RectTransform loRectTransform, Vector3 loPositionStart, Vector3 loPositionFinal, float lnSpeed, float lnPreDelay, float lnPosDelay)
	{
		isActive = true;
		loRectTransform.localPosition = loPositionStart;
		yield return new WaitForSeconds (lnPreDelay);
		float lnDelta = 0.0f;
		while (lnDelta < 1.0f)
		{
			lnDelta += lnSpeed * Time.deltaTime;//<< Actualizamos el delta.
			loRectTransform.localPosition = Vector3.Lerp (loPositionStart, loPositionFinal, lnDelta);//<< Nueva posicion.
			yield return null;//<< Esperamos siguiente actualizacion.
		}
		loRectTransform.localPosition = loPositionFinal;
		yield return new WaitForSeconds (lnPosDelay);
		isActive = false;
	}

	public void SetStart()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		positionStart = loRectTransform.localPosition;
	}

	public void SetFinal()
	{
		RectTransform loRectTransform = gameObject.GetComponent<RectTransform> ();
		positionFinal = loRectTransform.localPosition;
	}
}
