﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Animation2DScale))]
public class Animation2DScaleEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		Animation2DScale loObject = (Animation2DScale)target;
		if(GUILayout.Button("Set Start"))
		{
			loObject.SetStart();
		}
		else if(GUILayout.Button("Set Final"))
		{
			loObject.SetFinal();
		}
	}
}