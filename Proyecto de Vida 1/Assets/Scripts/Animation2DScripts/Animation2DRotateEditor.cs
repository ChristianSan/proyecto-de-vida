﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Animation2DRotate))]
public class Animation2DRotateEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		Animation2DRotate loObject = (Animation2DRotate)target;
		if(GUILayout.Button("Set Start"))
		{
			loObject.SetStart();
		}
		else if(GUILayout.Button("Set Final"))
		{
			loObject.SetFinal();
		}
	}
}
