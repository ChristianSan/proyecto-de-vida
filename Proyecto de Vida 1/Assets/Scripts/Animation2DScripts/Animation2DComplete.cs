﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof(TreeBranch))]
public class Animation2DComplete : MonoBehaviour 
{
	private bool gbAlive=true;							//<< Me indica si ya fue borrado el elemento.
	private int gnIndex=0;								//<< Me indica el indice en el que nos encontramos para inicializar el siguiente.
	private Animation2DTransition[] goTransitions=null;	//<< Seran nuestras transiciones.
	private GameObject goGameObjectLast=null;			//<< Ultimo GameObject.
	private GameObject goGameObjectCurrent=null;		//<< Actual GameObject.
	private TreeBranch goTreeBranch=null;				//<< Usamos nuestro tree branch para pasar al siguiente elemento.

	[Tooltip("Sera el indice dentro del componente TreeBranch para realizar el switch entre prefabs en caso de ser usado.")]
	public int indexOnTreeBranch=0;
	[Tooltip("Velocidad con la que se desvanece y aparece el siguiente objeto.")]
	public float fadeSpeed = 1.0f;
	[Tooltip("Son nuestros paneles que realizaran animaciones cada uno.")]
	public List<GameObject> prefabPanels;

	// Use this for initialization.
	void Start () 
	{
		goTreeBranch = gameObject.GetComponent<TreeBranch> ();

#if DEBUG
		if( prefabPanels.Count == 0) Debug.Log("No hay prefabs!!!");//<< No hay paneles.
		if (goTreeBranch == null) Debug.Log ("[Animation2dComplete]: Este componente no usa TreeBranch pero puede usarse como complemento.");
#endif

		CreateNextPrefab ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gbAlive)
		{
			//<< Como ya no tenemos transiciones activas simplemente pasamos a la siguiente transicion.
			CreateNextPrefab ();

			//<< Como ya no tenemos mas paneles creamos el objeto final y nos destruimos =D
			if (gnIndex >= prefabPanels.Count)
			{
				if(!TransitionsActive()) Skip();
			}
		}
	}

	public void Skip()
	{
		if (goTreeBranch != null) goTreeBranch.OnClick (indexOnTreeBranch);
		gbAlive = false;
	}

	private bool TransitionsActive()
	{
		//<< Cuando no haya mas transiciones en curso pasamos al siguiente panel.
		if (goTransitions != null)
		{
			foreach (Animation2DTransition loTransition in goTransitions)
			{
				if (loTransition.isActive) return true;//<< Como aun tenemos una transicion activa regresamos.
			}
		}
		return false;
	}

	private void CreateNextPrefab()
	{
		if (gnIndex >= prefabPanels.Count) return;//<< Si superamos los prefabs significa que ya no hay.
		if (TransitionsActive ()) return;//<< Checamos que las transiciones esten activas.

		//<< Iniciamos creando nuestro primer panel.
		GameObject loGameObject = Instantiate (prefabPanels [gnIndex]);
		loGameObject.transform.SetParent (gameObject.transform,false);
		goTransitions = loGameObject.GetComponentsInChildren<Animation2DTransition> ();//<< Obtenemos las transiciones de este elemento.
		gnIndex++;//<< Pasamos al siguiente...

		//<< Checamos si es el primero.
		if (goGameObjectCurrent == null)
		{
			goGameObjectCurrent = loGameObject;//<< Establecemos el ultimo.
			return;
		}

		goGameObjectLast = goGameObjectCurrent;
		goGameObjectCurrent = loGameObject;

		CanvasGroup loCanvasGroup = loGameObject.GetComponent<CanvasGroup> ();
		
		#if DEBUG
		if( loCanvasGroup == null ){ Debug.Log("[TreeBranch] No se encontro el CanvasGroup para realizar FadeOut en el gameObject: " + gameObject.name); return; }
		#else
		if( loCanvasGroup == null ) return;
		#endif

		StartCoroutine(CoroutineFadeTransition(true, loCanvasGroup, goGameObjectLast, fadeSpeed, true));
	}

	/// <summary>
	/// Coroutines to fade in canvas group.
	/// </summary>
	/// <returns>Nothing.</returns>
	/// <param name="loCanvasGroup">The canvas group.</param>
	IEnumerator CoroutineFadeTransition(bool IsFadeIn, CanvasGroup loCanvasGroup, GameObject loToDestroy, float lnSpeed, bool lbDestroy)
	{
		loCanvasGroup.interactable = false;	//<< Decimos que no podemos interactuar durante la transicion.
		
		if (IsFadeIn)
		{
			loCanvasGroup.alpha = 0.0f;
			float lnStep = 0.0f;
			while (lnStep < 1.0f)
			{
				loCanvasGroup.alpha = lnStep;			//<< Nuestro Alpha establecido con el paso del tiempo.
				lnStep += fadeSpeed * Time.deltaTime;	//<< [Distancia = Velocidad * Tiempo]
				yield return null;
			}
			loCanvasGroup.alpha = 1.0f;					//<< Establecemos el alpha al 100%.
		}
		else//<< FadeOut
		{
			loCanvasGroup.alpha = 1.0f;
			float lnStep = 1.0f;
			while (lnStep > 0.0f)
			{
				loCanvasGroup.alpha = lnStep;			//<< Nuestro Alpha establecido con el paso del tiempo.
				lnStep -= fadeSpeed * Time.deltaTime;	//<< [Distancia = Velocidad * Tiempo]
				yield return null;
			}
			
			loCanvasGroup.alpha = 0.0f;					//<< Establecemos el alpha al 0%.
		}
		
		loCanvasGroup.interactable = true;		//<< Indicamos que ya podemos interactuar con los elementos.
		if( lbDestroy ) Destroy (loToDestroy);	//<< Finalmente destruimos nuestro objeto de atras.
	}
}
